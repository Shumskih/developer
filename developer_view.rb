require_relative 'developer'
require_relative 'developer_dao'

class DeveloperView

  def console_view
    @developer_dao = DeveloperDAO.new
    @user = nil

    while @user != 6
      begin
        puts 'Choose, what do you want to do: '
        puts '    1. Create developer'
        puts '    2. Update developer'
        puts '    3. Find developer by id'
        puts '    4. Remove developer'
        puts '    5. Show all developers'
        puts '    6. Exit'

        @user = gets.chomp.strip

        case @user
        when '1'

          save_developer

        when '2'

          update_developer

        when '3'
          puts "Enter developer's id:"
          developer_id = gets.chomp.strip
          developer = @developer_dao.get_by_id(developer_id)
          puts '=================='
          puts 'Id: ' + developer.id
          puts 'Name: ' + developer.name
          puts 'Surname:' + developer.surname
          puts 'Specialization: ' + developer.specialization
          puts 'Experience: ' + developer.experience
          puts 'Salary: ' + developer.salary
          puts ' '

        when '4'

          puts "Enter developer's id"
          @developer_id = gets.chomp.strip

          @developer_dao.remove(@developer_id)

          puts "Developer removed by id: #{@developer_id}"

        when '5'

          @developer_dao.show_all

        when '6'
          puts 'Exiting'
          break
        else
          puts 'Oooopppppsssss'
        end
      end
    end
  end

  def save_developer
    begin
      puts "Enter developer's id"
      @developer_id = gets.chomp.strip
      if @developer_id.to_i.zero?
        puts 'It is not an integer'
      else
        puts 'It is integer'
        break
      end
    end while true

    begin
      puts "Enter developer's name"
      @developer_name = gets.chomp.strip
      if @developer_name.empty?
        puts 'Try again'
      else
        break
      end
    end while true

    begin
      puts "Enter developer's surname"
      @developer_surname = gets.chomp.strip
      if @developer_surname.empty?
        puts 'Try again'
      else
        break
      end
    end while true

    begin
      puts "Enter developer's specialization"
      @developer_specialization = gets.chomp.strip
      if @developer_specialization.empty?
        puts 'Try again'
      else
        break
      end
    end while true

    begin
      puts "Enter developer's experience"
      @developer_experience = gets.chomp.strip
      if @developer_experience.to_i.zero?
        puts 'It is not an integer'
      else
        puts 'It is integer'
        break
      end
    end while true

    begin
      puts "Enter developer's salary"
      @developer_salary = gets.chomp.strip
      if @developer_salary.to_i.zero?
        puts 'It is not an integer'
      else
        puts 'It is integer'
        break
      end
    end while true

    developer = Developer.new
    developer.id = @developer_id
    developer.name = @developer_name
    developer.surname = @developer_surname
    developer.specialization = @developer_specialization
    developer.experience = @developer_experience
    developer.salary = @developer_salary

    puts "#{developer.id},#{developer.name},#{developer.surname},#{developer.specialization},#{developer.experience},#{developer.salary}."
    @developer_dao.save(developer)
  end

  def update_developer
    begin
      puts "Enter developer's id you are going to update:"

      developer_id = gets.chomp.strip

      developer_exists = @developer_dao.developer_exist?(developer_id)

      if developer_exists
        puts 'Developer exists'

        puts 'What do you want to update?'
        puts '    1. Name'
        puts '    2. Surname'
        puts '    3. Specialization'
        puts '    4. Experience'
        puts '    5. Salary'
        puts '    6. All'
        puts '    7. Exit'

        @user = gets.chomp.strip

        update_developer_cases(@user, developer_id, @developer_dao)

      else
        puts 'Developer is not exists'
      end


    end while true
  end

  def update_developer_cases(user_input, developer_id, developer_dao)
    case user_input
    when '1'
      begin
        puts "Enter developer's name"
        @developer_name = gets.chomp.strip
        if @developer_name.empty?
          puts 'Try again'
        else
          developer_old = @developer_dao.get_by_id(developer_id)
          developer_new = Developer.new
          developer_new.id = developer_old.id
          developer_new.name = @developer_name
          developer_new.surname = developer_old.surname
          developer_new.specialization = developer_old.specialization
          developer_new.experience = developer_old.experience
          developer_new.salary = developer_old.salary

          @developer_dao.update(developer_new)

          puts "New developer's name is #{@developer_name}"
          break
        end
      end while true
    when '2'
      begin
        puts "Enter developer's surname"
        @developer_surname = gets.chomp.strip
        if @developer_surname.empty?
          puts 'Try again'
        else
          developer_old = @developer_dao.get_by_id(developer_id)
          developer_new = Developer.new
          developer_new.id = developer_old.id
          developer_new.name = developer_old.name
          developer_new.surname = @developer_surname
          developer_new.specialization = developer_old.specialization
          developer_new.experience = developer_old.experience
          developer_new.salary = developer_old.salary

          @developer_dao.update(developer_new)

          puts "New developer's name is #{@developer_surname}"
          break
        end
      end while true
    when '3'
      begin
        puts "Enter developer's specialization"
        @developer_specialization = gets.chomp.strip
        if @developer_specialization.empty?
          puts 'Try again'
        else
          developer_old = @developer_dao.get_by_id(developer_id)
          developer_new = Developer.new
          developer_new.id = developer_old.id
          developer_new.name = developer_old.name
          developer_new.surname = developer_old.surname
          developer_new.specialization = @developer_specialization
          developer_new.experience = developer_old.experience
          developer_new.salary = developer_old.salary

          @developer_dao.update(developer_new)

          puts "New developer's specialization is #{@developer_specialization}"
          break
        end
      end while true
    when '4'
      begin
        puts "Enter developer's experience"
        @developer_experience = gets.chomp.strip
        if @developer_experience.to_i.zero?
          puts 'It is not an integer'
        else
          developer_old = @developer_dao.get_by_id(developer_id)
          developer_new = Developer.new
          developer_new.id = developer_old.id
          developer_new.name = developer_old.name
          developer_new.surname = developer_old.surname
          developer_new.specialization = developer_old.specialization
          developer_new.experience = @developer_experience
          developer_new.salary = developer_old.salary

          @developer_dao.update(developer_new)

          puts 'It is integer'
          puts "New developer's experience is #{@developer_experience}"
          break
        end
      end while true
    when '5'
      begin
        puts "Enter developer's salary"
        @developer_salary = gets.chomp.strip
        if @developer_salary.to_i.zero?
          puts 'It is not an integer'
        else
          developer_old = @developer_dao.get_by_id(developer_id)
          developer_new = Developer.new
          developer_new.id = developer_old.id
          developer_new.name = developer_old.name
          developer_new.surname = developer_old.surname
          developer_new.specialization = developer_old.specialization
          developer_new.experience = developer_old.experience
          developer_new.salary = @developer_salary

          @developer_dao.update(developer_new)

          puts 'It is integer'
          puts "New developer's salary is #{@developer_salary}"
          break
        end
      end while true
    when '6'
      @developer_dao.remove(developer_id)
      save_developer
    end
  end
end
