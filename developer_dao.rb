require_relative 'developer_view'
require_relative 'developer'

class DeveloperDAO

  FILEPATH = 'developer.txt'
  TEMPFILEPATH = 'developer.txt.tmp'

  def save(developer)
    file = File.new(FILEPATH, 'a+')
    file.puts "#{developer.id},#{developer.name},#{developer.surname},#{developer.specialization},#{developer.experience},#{developer.salary}"
    file.close
  end

  def update(developer)
    rewrite_file(developer.id)
    File.rename(TEMPFILEPATH, FILEPATH)
    save(developer)
  end

  def remove(developer_id)
    rewrite_file(developer_id)
    File.rename(TEMPFILEPATH, FILEPATH)
  end

  def get_by_id(developer_id)
    IO.foreach(FILEPATH) do |line|
      developer_a = line.split(',')

      if developer_a[0] == developer_id
        @developer = Developer.new
        @developer.id = developer_a[0]
        @developer.name = developer_a[1]
        @developer.surname = developer_a[2]
        @developer.specialization = developer_a[3]
        @developer.experience = developer_a[4]
        @developer.salary = developer_a[5]
      end
    end
    return @developer
  end

  def show_all
    IO.foreach(FILEPATH) do |line|
      developer = line.split(',')
      puts '==================='
      puts "Id: #{developer[0]}"
      puts "Name: #{developer[1]}"
      puts "Surname: #{developer[2]}"
      puts "Specialization: #{developer[3]}"
      if developer[4] == '1'
        puts "Experience: #{developer[4]} year"
      else
        puts "Experience: #{developer[4]} years"
      end
      puts "Salary: #{developer[5]}"
      puts ' '
    end
  end

  def developer_exist?(developer_id)
    IO.foreach(FILEPATH) do |line|
      developer = line.split(',')

      return developer[0] if developer[0] == developer_id
    end
  end

  def rewrite_file(developer_id)
    File.open(FILEPATH, 'r') do |f|
      File.open(TEMPFILEPATH, 'w') do |f2|
        f.each_line do |line|
          f2.write(line) unless line.start_with? developer_id
        end
      end
    end
  end
end